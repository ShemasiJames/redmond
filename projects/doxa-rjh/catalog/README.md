
# doxa-liml catalog 
Information about projects being made public for use with Doxa. If a project is not listed here, even if it is a Gitlab public project, do not use it.

The files are © The Orthodox Christian Mission Center

Use of these files is subject to the terms stated in the LICENSE file.

[Doxa](https://doxa.liml.org) is a liturgical software product from the [Orthodox Christian Mission Center](https://ocmc.org). 

[Doxa install link](https://github.com/liturgiko/doxa/releases)
